var fs = require('fs')
    , path = require('path')
    , Sequelize = require('sequelize')
    , lodash = require('lodash')
    , db = {};
if (process.env.DATABASE_URL){
    var sequelize = new Sequelize(process.env.DATABASE_URL, {
            dialect: 'postgres',
            protocol: 'postgres'
        }
    );
}else{
    var sequelize = new Sequelize('wineDB', 'postgres', 'neruda23',
        {
            dialect: 'postgres'
        })
};
fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return (file.indexOf('.') !== 0) && (file !== 'index.js') && (file !== 'database.js')
    })
    .forEach(function(file) {
        var model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model
    });

module.exports = lodash.extend({
    sequelize: sequelize,
    Sequelize: Sequelize
}, db);