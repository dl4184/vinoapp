var bcrypt = require('bcrypt-nodejs')

module.exports = function (sequelize, DataTypes) {
    var User = sequelize.define('user', {
            email:{type: DataTypes.STRING},
            username:{type: DataTypes.STRING},
            password:{type: DataTypes.STRING},
            conformationId:{type: DataTypes.STRING},
            conformation:{type: DataTypes.BOOLEAN, defaultValue: false},
            resetPasswordToken: {type: DataTypes.STRING},
            resetPasswordExpires: {type: DataTypes.DATE}
        }
        , {
            instanceMethods: {
                generateHash: function (password, done) {
                    bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
                        bcrypt.hash(password, salt, null, done);
                    });
                },

                validPassword: function (password, next) {
                    bcrypt.compare(password, this.password, next)
                }
            }
        });
    sequelize.sync()
    User.beforeCreate(function (user,options, done) {
        user.generateHash(user.password, function (err, encrypted) {
            if (err) return done(err);
            user.password = encrypted;
            done(null, user);
        })
    });
    return User

};