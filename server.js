var express = require("express"),
	app = express(),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
	parseUrl = require('parseurl'),
    session = require('express-session'),
    db = require('./models'),
	pg = require('pg'),
    http = require('http'),
    passport = require('passport'),
    flash = require('connect-flash'),
    handlebars = require('express-handlebars'),
    expressValidator = require('express-validator'),
    user = require('./routes/user'),
    routes = require('./routes/index'),
	path = require('path');


SALT_WORK_FACTOR = 12;

//Server configuration
var config = require('./config/config.js');
var serverPort=config["server"]["port"];

//View Engine
app.set('views', __dirname + '/views');
app.engine('handlebars', handlebars({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');




var helmet = require('helmet');
app.use(helmet());

//BodyParser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

//Express Session


require('./config/passport')(passport);


app.use(function (req, res, next) {
    console.log("/" + req.method+" "+req.url);
    next();
});

app.get('/', function (req, res) {
    res.render('home');
});

app.get('/about', function (req, res) {
    res.render('about');
});

app.get('/home', function (req, res) {
    res.render('home');
});


app.get('/forgot', function(req, res) {
    res.render('forget', {
        user: req.user
    });
});

app.get('/vina/:vinoId', function (req, res) {
    res.render('vina');
});


app.get('/reset', function(req, res) {
    res.render('reset', {
        user: req.user
    });
});
app.get('/logout', function(req, res) {
    res.render('login', {
        user: req.user
    });
});
app.get('/mojaVina', function(req, res) {
    res.render('mojaVina', {layout: 'session'});
});

// Connect Flash
app.use(flash());


// Express Validator
app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var namespace = param.split('.')
            , root    = namespace.shift()
            , formParam = root;

        while(namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param : formParam,
            msg   : msg,
            value : value
        };
    }
}));


//Setting to Sign up/Login
app.use('/', user);



app.use('/', routes);


app.use(function(req, res, next){
  res.status(404);

  // respond with html page
  if (req.accepts('html')) {
    res.render('404', { url: req.url });
    return;
  }

  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send('Not found');
});

app.use(function(req, res, next){
  res.status(500);

  // respond with html page
  if (req.accepts('html')) {
    res.render('500', { url: req.url });
    return;
  }

  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Internal Server Error' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send('Internal Server Error');
});

// Global Vars
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});


//Set Port
app.set('port', process.env.PORT || serverPort);

//Conection to port

db
    .sequelize
    .authenticate()
    .then(function(err){
        if(err){
            throw err[0]
        }
        else {
            app.listen(app.get('port'), function () {
                console.log('Express is listening on port ' + app.get('port'))
            })
        }
    });