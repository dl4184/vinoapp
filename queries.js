var promise = require('bluebird');

var options = {
    // Initialization Options
    promiseLib: promise
};
var config = require('./config/config.js');
var pgp = require('pg-promise')(options);
var connectionString = config["database"];
var db = pgp(connectionString);


//Query functions

module.exports = {
    getAllWines: getAllWines,
    getSingleWine: getSingleWine,
    removeWine: removeWine,
    getSingleWineName: getSingleWineName,
    getWineNames: getWineNames,
    updateWine: updateWine,
    createWine: createWine
};

//GET za vsa vina
function getAllWines(req, res, next) {
    db.any('select * from wine')
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved ALL wines'
                });
        })
        .catch(function (err) {
            return next(err);
        });
}

function getWineNames(req, res, next) {
    db.any('select id, name, alcohol_level, style, vintage, color, sugar_level from wine order by name')
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved ALL wine names'
                });
        })
        .catch(function (err) {
            return next(err);
        });

}

//Get za vino glede na id
function getSingleWine(req, res, next) {
    var wineID = parseInt(req.params.id);
    db.one('select * from wine where id = $1', wineID)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved ONE wine'
                });
        })
        .catch(function (err) {
            return next(err);
        });
}

/*Get za vino glede na ime*/
function getSingleWineName(req, res, next) {
    var wineName = (req.params.name);
    console.log(wineName);
    db.any('select * from wine where name = $1', wineName)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved wine by a name'
                });
        })
        .catch(function (err) {
            return next(err);
        });
}


/*DELETE*/
function removeWine(req, res, next) {
    var wineID = parseInt(req.params.id);
    db.result('delete from wine where id = $1', wineID)
        .then(function (result) {
            /* jshint ignore:start */
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Removed wine'+wineID
                });
            /* jshint ignore:end */
        })
        .catch(function (err) {
            return next(err);
        });
}

/*UPDATE*/
function updateWine(req, res, next) {

    var wineID = parseInt(req.params.id);
    db.one('select * from wine where id = $1', wineID)
        .then(function (data) {


            var name = req.body.name || data["name"] ,
                wine_grower = req.body.wine_grower ||data["wine_grower"] ,
                region = req.body.region ||data["region"] ,
                wine_type = req.body.wine_type||data["wine_type"] ,
                vintage = req.body.vintage||data["vintage"] ,
                style = req.body.style||data["style"] ,
                color = req.body.color||data["color"] ,
                body = req.body.body||data["body"] ,
                sugar_level = req.body.sugar_level||data["sugar_level"] ,
                alcohol_level = req.body.alcohol_level||data["alcohol_level"] ,
                recommended_temperature = req.body.recommended_temperature||data["recommended_temperature"] ,
                production = req.body.production||data["production"] ,
                aging_of_wine = req.body.aging_of_wine||data["aging_of_wine"] ,
                image_url = req.body.image_url||data["image_url"] ,
                description = req.body.description||data["description"] ;


            db.none('update wine set name=$1, wine_grower=$2, region=$3, wine_type=$4, vintage=$5, style=$6 ,color=$7, body=$8, sugar_level=$9, alcohol_level=$10, recommended_temperature=$11, production=$12, aging_of_wine=$13, image_url=$14, description=$15 where id=$16',
                [name, wine_grower, region, wine_type, vintage, style, color, body, sugar_level, alcohol_level, recommended_temperature, production, aging_of_wine, image_url, description, wineID])
                .then(function () {
                    res.status(200)
                        .json({
                            status: 'success',
                            message: 'Updated wine'
                        });
                })
                .catch(function (err) {
                    return next(err);
                });
        }).catch(function (err) {
        return next(err);
    });
}

/*CREATE*/
function createWine(req, res, next) {
    db.none('insert into wine(id,name, wine_grower, region, wine_type, vintage, style, color, body, sugar_level, alcohol_level, recommended_temperature, production, aging_of_wine, image_url, description)' +
        'values((SELECT MAX(id) FROM wine)+1,${name}, ${wine_grower}, ${region}, ${wine_type}, ${vintage}, ${style}, ${color}, ${body}, ${sugar_level}, ${alcohol_level}, ${recommended_temperature}, ${production}, ${aging_of_wine}, ${image_url}, ${description})',
        req.body)
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Inserted one wine'
                });
        })
        .catch(function (err) {
            return next(err);
        });
}

