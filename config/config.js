var config = {
    database: {
		user: 'postgres', //env var: PGUSER
		password: 'neruda23', //env var: PGPASSWORD
        port: 5432, //env var: PGPORT
		host: 'localhost', // Server hosting the postgres database
		database: 'wineDB', //env var: PGDATABASE
		max: 10, // max number of clients in the pool
		idleTimeoutMillis: 10000 // how long a client is allowed to remain idle before being closed
	},
    server: {
        host: '127.0.0.1',
        port: '3000'
    },
    RESTAuth:{
    	user:'admin',
		password: 'neruda23'
	}
};
module.exports = config;