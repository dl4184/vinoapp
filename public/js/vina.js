//AJAX za polnjenje podatkov
var katalog = null;
$(document).ready(function () {
    var url = window.location.pathname;
    var urlArray = url.split("/");
    var indexVina = urlArray.indexOf('vina');
    if (indexVina >= 0) {
        var vinoId = 1;
        if (indexVina + 1 < urlArray.length)
            vinoId = urlArray[indexVina + 1];


        $.ajax({
            type: "GET",
            url: "/api/wine/" + vinoId,
            contentType: "application/json",
            success: function (response) {
                katalog = response["data"];

                $("#ime-id").text(katalog["name"]);
                $("#opis-id").text(katalog["description"]);
                $("#letnik-id").text(katalog["vintage"]);
                $("#sladkor-id").text(katalog["sugar_level"]);
                $("#sorta-id").text(katalog["wine_type"]);
                $("#barva-id").text(katalog["color"]);
                $("#alkohol-id").text(katalog["alcohol_level"] + " %");
                $("#stil-id").text(katalog["style"]);
                $("#telo-id").text(katalog["body"]);
                $("#vinar-id").text(katalog["wine_grower"]);
                $("#okolis-id").text(katalog["region"]);
                $("#pridelava-id").text(katalog["production"]);
                $("#temperatura-id").text(katalog["recommended_temperature"] + " °C");
                $("#slika-id").attr('src', katalog["image_url"]);
            },
            error: function (response) {
                console.log("404");
            }
        });
    }


    $.ajax({
        type: "GET",
        url: "/api/wine/names",
        contentType: "application/json",
        success: function (response) {
            katalog = response["data"];
            //zanka, ki gre skozi vsak element v seznamu
            for (var i = 0; i < Object.keys(katalog).length; i++) {
                var prvaCrka = katalog[i]["name"][0].toLowerCase();//tako pridobimo prvo črko in jo nastavimo na lower case
                $("#div-crka" + prvaCrka).append("<li class='povezava'><a href='/vina/" + katalog[i]["id"] + "' data-id='" + i + "'>" + katalog[i]["name"] + "</a></li>");
            }
            clearLetters();

        },
        error: function (response) {
            console.log("404");
        }
    });


    /*search box*/

    /*var submitIcon = $('.searchbox-icon');
     var inputBox = $('.searchbox-input');
     var searchBox = $('.searchbox');
     var isOpen = false;

     submitIcon.click(function(){
     if(isOpen == false){
     inputBox.css("visibility", "visible");
     inputBox.animate({width: '235'}, 1000, function(){
     inputBox.focus();
     });


     isOpen = true;
     } else {
     inputBox.animate({width: '0'}, 1000, function(){
     inputBox.css("visibility", "hidden");
     inputBox.focusout();
     });

     isOpen = false;
     }
     });

     */
    /*end of search box*/
    var elementAlcoholExists = document.getElementById("alcohol-keypress");
    var elementYearExists = document.getElementById("year-keypress");

    if (elementAlcoholExists != null)
        createSlider('alcohol-keypress', 'input-alcohol-keypress-0', 'input-alcohol-keypress-1', 0, 20, 1, '');
    if (elementYearExists != null)
        createSlider('year-keypress', 'input-year-keypress-0', 'input-year-keypress-1', 2000, 2017, 0, '');
});


function clearLetters() {
    var len, letter;
    var divs = $('.letter');
    divs.each(function () {
        len = $(this).children().filter(function () {
            return $(this).css('display') !== 'none';
        }).length;
        letter = $(this).attr('id').substr(-1).toUpperCase();
        if (len == 1) {
            $(this).css("display", "none");
            $("#letter-link-" + letter).addClass("nolink");
        }
        else {
            $(this).css("display", "block");
            $("#letter-link-" + letter).removeClass("nolink");
        }
    })
}

function filterWines() {
    console.log("filter");
    var input, filter, li, a, i, id, colorMatches, styleMatches, sugarMatches, vintageMatch, year, alcoholMatch, alcohol;
    input = $('#search-input');
    filter = input.val().toUpperCase();
    li = $('.povezava');
    var isRed = $("#color-red").is(":checked");
    var isRose = $("#color-rose").is(":checked");
    var isWhite = $("#color-white").is(":checked");

    var isCalm = $("#style-calm").is(":checked");
    var isBubbly = $("#style-bubbly").is(":checked");


    var sugarNone = $("#sugar-none").is(":checked");
    var sugarWith = $("#sugar-with").is(":checked");

    var minAlcohol = parseFloat($("#input-alcohol-keypress-0").val());
    var maxAlcohol = parseFloat($("#input-alcohol-keypress-1").val());

    var validYearInput = false;
    if (!isNaN($("#input-year-keypress-0").val()) && !isNaN($("#input-year-keypress-1").val())) {
        var minYear = parseInt($("#input-year-keypress-0").val());
        var maxYear = parseInt($("#input-year-keypress-1").val());
        validYearInput = true;
    }

    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        id = a.getAttribute("data-id");
        colorMatches = true;
        switch (katalog[id]["color"]) {
            case "rdeče":
                if (isRed)
                    colorMatches = true;
                else
                    colorMatches = false;
                break;
            case "rose":
                if (isRose)
                    colorMatches = true;
                else
                    colorMatches = false;
                break;
            case "belo":
                if (isWhite)
                    colorMatches = true;
                else
                    colorMatches = false;
                break;
        }

        styleMatches = true;
        switch (katalog[id]["style"]) {
            case "mirno":
                if (isCalm)
                    styleMatches = true;
                else
                    styleMatches = false;
                break;
            case "peneče":
                if (isBubbly)
                    styleMatches = true;
                else
                    styleMatches = false;
                break;
        }

        sugarMatches = true;
        switch (katalog[id]["sugar_level"]) {
            case "suho":
                if (sugarNone)
                    sugarMatches = true;
                else
                    sugarMatches = false;
                break;
            case "z ostankom sladkorja":
                if (sugarWith)
                    sugarMatches = true;
                else
                    sugarMatches = false;
                break;
        }

        alcoholMatch = true;
        alcohol = katalog[id]["alcohol_level"];
        alcohol = alcohol.replace(/^\D+/g, '');
        alcohol = alcohol.replace(",", ".");
        if (alcohol.length > 0) {
            alcohol = parseFloat(alcohol);
            if (alcohol >= minAlcohol && alcohol <= maxAlcohol)
                alcoholMatch = true;
            else
                alcoholMatch = false;
        }

        vintageMatch = true;
        if (validYearInput) {
            year = katalog[id]["vintage"];
            if (!isNaN(year)) {
                year = parseInt(year);

                if (year >= minYear && year <= maxYear)
                    vintageMatch = true;
                else
                    vintageMatch = false;
            }
        }


        if (a.innerHTML.toUpperCase().indexOf(filter) > -1 && colorMatches && styleMatches && sugarMatches && vintageMatch && alcoholMatch) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
        //li=li.concat(div[i].getElementsByTagName("p"));
        /*<p><a></a></p>*/
    }
    clearLetters();
}
/*'alcohol-keypress','input-alcohol-keypress-0','input-alcohol-keypress-1',min,max,1,' (%)'*/
function createSlider(keypressId, input0Id, input1Id, min, max, decimals, postfix) {
    var keypressSlider = document.getElementById(keypressId);
    var input0 = document.getElementById(input0Id);
    var input1 = document.getElementById(input1Id);
    var inputs = [input0, input1];

    noUiSlider.create(keypressSlider, {
        start: [min, max],
        connect: true,
        direction: 'ltr',
        /*tooltips: [true, wNumb({ decimals: 1 })],*/
        range: {
            'min': min,
            'max': max
        },
        format: wNumb({
            decimals: decimals,
            postfix: postfix,
        })
    });

    keypressSlider.noUiSlider.on('update', function (values, handle) {
        inputs[handle].value = values[handle];
        filterWines()
    });

    function setSliderHandle(i, value) {
        var r = [null, null];
        r[i] = value;
        keypressSlider.noUiSlider.set(r);
    }


    // Listen to keydown events on the input field.
    inputs.forEach(function (input, handle) {

        input.addEventListener('change', function () {
            setSliderHandle(handle, this.value);
        });

        input.addEventListener('keydown', function (e) {

            var values = keypressSlider.noUiSlider.get();
            var value = Number(values[handle]);

            // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
            var steps = keypressSlider.noUiSlider.steps();

            // [down, up]
            var step = steps[handle];

            var position;

            // 13 is enter,
            // 38 is key up,
            // 40 is key down.
            switch (e.which) {

                case 13:
                    setSliderHandle(handle, this.value);
                    break;

                case 38:

                    // Get step to go increase slider value (up)
                    position = step[1];

                    // false = no step is set
                    if (position === false) {
                        position = 1;
                    }

                    // null = edge of slider
                    if (position !== null) {
                        setSliderHandle(handle, value + position);
                    }

                    break;

                case 40:

                    position = step[0];

                    if (position === false) {
                        position = 1;
                    }

                    if (position !== null) {
                        setSliderHandle(handle, value - position);
                    }

                    break;
            }
        });
    });

}

