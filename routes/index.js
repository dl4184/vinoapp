var express = require('express');
var router = express.Router();

/*RESTful API*/
var db = require('../queries');

var config = require('../config/config.js');
var RESTAuth=config["RESTAuth"];

//Authorization for REST services
var basicAuth = require('basic-auth');

var auth = function (req, res, next) {
    function unauthorized(res) {
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.send(401);
    };

    var user = basicAuth(req);

    if (!user || !user.name || !user.pass) {
        return unauthorized(res);
    };

    if (user.name === RESTAuth["user"] && user.pass === RESTAuth["password"]) {
        return next();
    } else {
        return unauthorized(res);
    };
};


router.get('/api/wine/names', db.getWineNames);
router.get('/api/wine', db.getAllWines);
router.get('/api/wine/:id', db.getSingleWine);
router.get('/api/wine/name/:name', db.getSingleWineName);
router.put('/api/wine/:id',auth, db.updateWine);
router.post('/api/wine/',auth, db.createWine);
router.delete('/api/wine/:id',auth, db.removeWine);

module.exports = router;
