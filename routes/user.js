var db = require('../models');
var express = require('express');
var router = express.Router();
var passport = require('passport');
var flash = require('connect-flash');
var user = require('../models/user');
var nodemailer = require("nodemailer");
var config = require('../config/config.js');
var expressValidator = require('express-validator');
var async = require('async');
var crypto = require('crypto');
var session = require('express-session');
var bodyParser = require('body-parser');

router.use(session({
    cookieName: 'session',
    secret: 'goatjsformakebettersecurity',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
    resave: true,
    saveUninitialized: true
}));


router.get('/katalog', function (req, res) {
    console.log(req.session.cookie);
    console.log(req.session.username);
    if (req.session && req.session.username) { // Check if session exists
        // lookup the user in the DB by pulling their email from the session
        db.user.find({
            where: {
                'username': req.session.username
            }
        }).then(function (user) {
            if (!user) {
                // if the user isn't found in the DB, reset the session info and
                // redirect the user to the login page
                req.session.reset();
                res.redirect('/login');
            } else {
                // expose the user to the template
                res.locals.user = user;
                // render the dashboard page
                res.render('katalog', {layout: 'session'});
            }
        });
    } else {
        res.redirect('/login');
    }
});


//Conformation Mail - init
var smtpTransport = nodemailer.createTransport("SMTP", {
    service: "Gmail",
    auth: {
        user: "vinoapp2017@gmail.com",
        pass: "neruda23"
    }
});
var rand, mailOptions, host, link;

// Register
router.get('/signup', function (req, res) {
    res.render('signup');
});
//Login
router.get('/login', function (req, res) {
    res.render('login')
});
// tukaj so narejenji customValidatorji za email in username -> ce ze obstaja email oz username nam prozi napako
router.use(expressValidator({
    customValidators: {
        isUsernameAvailable: function (username) {
            return new Promise(function (resolve, reject) {
                db.user.find({
                    where: {'username': username}
                }).then(function (user) {
                    if (user == null) {
                        resolve();
                    } else {
                        reject();
                    }
                });

            });
        },
        isEmailAvailable: function (email) {
            return new Promise(function (resolve, reject) {
                db.user.find({
                    where: {'email': email}
                }).then(function (email) {
                    if (email == null) {
                        resolve();
                    } else {
                        reject();
                    }
                });

            });
        },
        isUsernameInBase: function (username) {
            return new Promise(function (resolve, reject) {
                db.user.find({
                    where: {'username': username}
                }).then(function (user) {
                    if (user != null) {
                        resolve();
                    } else {
                        reject();
                    }
                });
            });
        },
        isEmailConfirmed: function (username) {
            return new Promise(function (resolve, reject) {
                if (username.length == 0)
                    resolve();
                db.user.find({
                    where: {
                        'username': username,
                        'conformation': true
                    }
                }).then(function (user) {
                    if (user != null) {
                        resolve();
                    } else {
                        reject();
                    }
                });
            });
        }
    }

}));

router.use(expressValidator({
    customValidators: {
        isUsernameAvailable: function (username) {
            return new Promise(function (resolve, reject) {
                db.user.find({
                    where: {'username': username}
                }).then(function (user) {
                    if (user == null) {
                        resolve();
                    } else {
                        reject();
                    }
                });

            });
        },
        isEmailAvailable: function (email) {
            return new Promise(function (resolve, reject) {
                db.user.find({
                    where: {'email': email}
                }).then(function (email) {
                    if (email == null) {
                        resolve();
                    } else {
                        reject();
                    }
                });

            });
        },
        isEmailConfirmed: function (username) {
            return new Promise(function (resolve, reject) {
                var criteria = username.indexOf('@') > -1 ? {email: username, conformation: true} : {
                        username: username,
                        conformation: true
                    };
                db.user.find({
                    where: criteria
                }).then(function (user) {
                    if (user != null) {
                        resolve();
                    } else {
                        reject();
                    }
                });
            });
        },
        isAuthenticationCorrect: function (username, password) {
            return new Promise(function (resolve, reject) {
                var criteria = username.indexOf('@') > -1 ? {email: username} : {username: username};
                db.user.findOne({
                    where: criteria
                }).then(function (user) {
                    if (!user.conformation)
                        resolve();
                    if (user) {
                        user.validPassword(password, function (err, isMatch) {
                            if (err) throw err;
                            if (isMatch)
                                resolve();
                            else
                                reject();
                        });
                    }
                    else {
                        reject();
                    }
                });
            });
        },
        isPasswordValid: function (password) {
            var pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/;
            var result = pattern.test(password);
            return result;
        },
        isUserNameValid: function (username) {
            var pattern = /^[A-Za-z0-9_.]{3,15}$/;
            var result = pattern.test(username);
            return result;
        }
    }

}));
function makeid(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
// Register User
router.post('/register', function (req, res, next) {
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var password2 = req.body.password2;

    // Validation
    req.checkBody('email', 'Vnesite email naslov').notEmpty();
    req.checkBody('email', 'Email naslov ni veljaven').isEmail();
    req.checkBody('email', 'Email ' + email + ' je že zaseden').isEmailAvailable();

    req.checkBody('username', 'Vnesite uporabniško ime').notEmpty();
    if (username.length > 0) {
        req.checkBody('username', 'Uporabniško ime ' + username + ' je že zasedeno').isUsernameAvailable();
        req.checkBody('username', 'Uporabniško ime ni veljavno. Uporabniško ime je lahko dolgo od 3 do 15 znakov in vsebuje naslednje znake 0-9, A-Z, a-z, . in _ .').isUserNameValid();
    }
    req.checkBody('password', 'Vnesite geslo').notEmpty();
    if (password.length > 0)
        req.checkBody('password', 'Geslo ni veljavno').isPasswordValid();
    req.checkBody('password2', 'Gesli se ne ujemata').equals(req.body.password);


    req.asyncValidationErrors().then(function () {
            rand = makeid(15);
            host = req.get('host');
            link = "http://" + req.get('host') + "/verify?id=" + rand;
            mailOptions = {
                to: email,
                subject: "Potrditev računa VinoApp",
                html: "Pozdravljeni,<br> vi ali nekdo drug je na tem emailu registriral VinoApp račun.<br> V primeru, da to niste bili vi to sporočilo enostavno ignorajte drugače, pa <br><a href=" + link + ">kliknite tukaj za aktivacijo računa.</a><br><br> Lep Pozdrav <br> VinoApp"
            }
            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                    res.end("error");
                } else {
                    res.render("conformation");
                }
            });
            db.user.create({
                email: email,
                username: username,
                password: password,
                conformationId: rand
            });
        }
    ).catch(function (errors) {

        if (errors) {
            res.render('signup', {
                errors: errors
            });

        }
    });
});

router.get('/verify', function (req, res) {
    //Odstrani zakomentiran if stavek spodaj, ne vem tocno zakaj se uporablja -> vcasih je povzrocal tezave zato sem ga zakomnetiral
    //if ((req.protocol + "://" + req.get('host')) == ("http://" + host)) {
    //najde uporabnika in ga posodobi conformation na true
    db.user.find({
        where: {
            conformationId: req.query.id,
        }
    }).then(function (user) {
        if (user) {
            user.updateAttributes({
                conformation: true,
            }).then(function (user) {
                res.redirect('login');
            });
        }
    });

});

router.post('/login', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    // Validation

    req.checkBody('username', 'Vnesite uporabniško ime ali geslo').notEmpty();
    req.checkBody('password', 'Vnesite geslo').notEmpty();

    req.checkBody('username', 'Uporabnik ' + username + ' še ni potrjen. Poglejte svoj email in ga potrdite.').isEmailConfirmed();

    req.check('username', 'Napačno uporabniško ime/email in/ali geslo').isAuthenticationCorrect(password);

    req.asyncValidationErrors().then(function () {
        req.session.username = username;
        res.redirect('katalog');
    }).catch(function (errors) {
        if (errors) {
            res.render('login', {
                errors: errors
            });
        }
    });

});

router.get("/logout", function (req, res) {
    console.log("loged out");
    req.session.destroy(function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log("loged out");
            req.end();
            res.redirect('/');
        }
    });

});

router.post('/forgot', function (req, res, next) {
    async.waterfall([
        function (done) {
            crypto.randomBytes(20, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {

            db.user.find({
                where: {
                    email: req.body.email
                }
            }).then(function (user, err) {
                if (user) {
                    user.updateAttributes({
                        resetPasswordToken: token,
                        resetPasswordExpires: Date.now() + 3600000 // 1 hour
                    }).then(function (user) {
                        done(err, token, user);
                    });
                }
                else {
                    res.redirect('forgot')
                }
            });
        },
        function (token, user, done) {
            var smtpTransport = nodemailer.createTransport('SMTP', {
                service: "Gmail",
                auth: {
                    user: "vinoapp2017@gmail.com",
                    pass: "neruda23"
                }
            });
            var mailOptions = {
                to: user.email,
                from: 'vinoapp2017@gmail.com"',
                subject: 'Pozabljeno geslo',
                text: 'Pozdravljeni, \n\n to sporočilo ste dobili, ker ste vi ali pa je nekdo drug zaprosil za ponastavitev gesla za vaš VinoApp račun.\n\n' +
                'Prosim obiščite spodnjo povezavo, da dokončate proces:\n\n' +
                'http://' + req.headers.host + '/reset/' + token + '\n\n' +
                'V pg promicerimeru, da niste zapg promicerosili za pg promiceonastavitev gesla to sporočilo ignorirajte.\n\n' +
                'Lep pozdrav\n VinoApp'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
            });
            res.render('passwordresetrequest');
        }
    ], function (err) {
        if (err) return next(err);
    });
});

router.get('/reset/:token', function (req, res) {
    db.user.find({
        where: {
            resetPasswordToken: req.params.token
        }
    }).then(function (user, err) {
        if (err)
            console.log(err)
        if (!user) {
            req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect('/forgot');
        }
        res.render('reset', {
            user: req.user
        });
    });
});


router.post('/reset/:token', function (req, res) {
    async.waterfall([
        function (done) {
            db.user.find({
                where: {
                    resetPasswordToken: req.params.token,
                }
            }).then(function (user, err) {
                var password = req.body.password;
                if (user) {
                    user.generateHash(password, function (err, encrypted) {
                        if (err) return done(err);
                        user.password = encrypted;
                        user.updateAttributes({
                            password: user.password,
                            resetPasswordToken: undefined,
                            resetPasswordExpires: '2100-01-01'
                        }).then(function (user) {
                            done(err, user);
                        });
                    });
                }
            });
        }, function (user, done) {
            var smtpTransport = nodemailer.createTransport('SMTP', {
                service: 'Gmail',
                auth: {
                    user: 'vinoapp2017@gmail.com',
                    pass: 'neruda23'
                }
            });
            var mailOptions = {
                to: user.email,
                from: 'vinoapp2017@gmail.com',
                subject: 'Vaše geslo je bilo spremenjeno',
                text: 'Pozdravljeni,\n\n' +
                'potrjujemo, da ste uspešno spremenili geslo za račun: ' + user.email + '\n' + '\n\n' + 'Lep pozdrav\n VinoApp'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                req.flash('success_msg', 'You are registered and can now login');

            });
            res.redirect('/login');
        }
    ], function (err) {
        if (err) return next(err);
    });
});

module.exports = router;